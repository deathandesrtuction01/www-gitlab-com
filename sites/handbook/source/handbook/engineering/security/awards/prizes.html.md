---
layout: handbook-page-toc
title: "Security Awards Prizes"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Prizes FY22

## Yearly

 | Category  | Rank   | Prize                          |
 | --------  | ---:   | -----                          |
 | All       | 1      | $1000 Amazon Gift Card         |
 | All       | 2-250  | GitLab Security Ninja Sticker  |

## FY22-Q1

 | Category         | Rank   | Prize                  |
 | --------         | ---:   | -----                  |
 | Development      | 1-10   | $100 Amazon Gift Card  |
 | Development      | 11-25  | $20 gitlab swag shop   |
 | Engineering      | 1-3    | $100 Amazon Gift Card  |
 | Non-Engineering  | 1-3    | $100 Amazon Gift Card  |
 | Community        | 1      | $100 Amazon Gift Card  |

# Prizes FY21

## FY21-Q4

 | Category         | Rank  | Prize                  |
 | --------         | ---:  | -----                  |
 | Development      | 1-5   | $100 Amazon Gift Card  |
 | Development      | 6-25  | $20 gitlab swag shop   |
 | Engineering      | 1-5   | $20 gitlab swag shop   |
 | Non-Engineering  | 1-5   | $20 gitlab swag shop   |
 | Community        | 1-2   | $100 Amazon Gift Card  |
